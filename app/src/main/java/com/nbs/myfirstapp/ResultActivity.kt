package com.nbs.myfirstapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        supportActionBar?.apply {
            title = "Result"
            setDisplayHomeAsUpEnabled(true)
        }

        val result = intent.
            getDoubleExtra("hasil", 0.0)
        tvResult.text = "Hasilnya adalah $result"

        btnOpenHelp.setOnClickListener {
            startActivity(Intent(this, HelpActivity::class.java))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
