package com.nbs.myfirstapp

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_help.*

class HelpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)

        supportActionBar?.apply {
            title = "Help"
            setDisplayHomeAsUpEnabled(true)
        }

        btnDialPhone.setOnClickListener {
            dialNumber()
        }

        btnShare.setOnClickListener {
            shareSomething()
        }

        btnOpenWebsite.setOnClickListener {
            openWebsite()
        }
    }

    private fun shareSomething() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Hello")
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Download tix.id untuk nonton bioskop lebih seru")
        shareIntent.setType("text/plain")
//        shareIntent.setPackage("com.whatsapp")
        startActivity(shareIntent)

//        try{
//            startActivity(shareIntent)
//        }catch (e: ActivityNotFoundException){
//            Toast.makeText(this,
//                "App is not found", Toast.LENGTH_SHORT)
//                .show()
//        }
    }

    private fun openWebsite() {
        val openWebIntent = Intent(Intent.ACTION_VIEW,
                Uri.parse("https://google.com"))
        startActivity(openWebIntent)
    }

    private fun dialNumber() {
        val dialIntent = Intent(Intent.ACTION_DIAL,
            Uri.parse("tel:0895611918545"))
        startActivity(dialIntent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
