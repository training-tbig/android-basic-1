package com.nbs.myfirstapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.apply {
            title = "Hitung Luas Bangun"
        }

        btnHitung.setOnClickListener {
            val panjang = edtPanjang.text.toString().trim()
            val lebar = edtLebar.text.toString().trim()

            if (panjang.isEmpty() || lebar.isEmpty()){
                Toast.makeText(this,
                        "Semua harus terisi", Toast.LENGTH_SHORT)
                        .show()
            }else{
                val result = hitung(panjang.toDouble(),
                        lebar.toDouble())

//                Toast.makeText(this,
//                        "Hasilnya $result",
//                        Toast.LENGTH_SHORT).show()

                val intent = Intent(this,
                    ResultActivity::class.java)
                intent.putExtra("hasil", result)
                startActivity(intent)

            }
        }
    }

    private fun hitung(panjang: Double, lebar: Double)
            = panjang * lebar
}
